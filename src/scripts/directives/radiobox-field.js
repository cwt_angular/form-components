'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtRadioboxField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtRadioboxfield.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            readMode:'=?',
            errorMessage:'@',
            errorPlacement:'@',
            label:'@',
            ngClick:'&',
            isLoading:'=',
            data:'=',
            name:'@',
            colClass:'=?',
            rowClass:'=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithPropertie  = function(){
            if(scope.ngModel){
              return scope.ngModel[scope.ngModelPropertie]
            }
            return "";
          }

          if(Array.isArray(scope.ngModel)){
            scope.ngModel = "";
          }
          scope.dataHelp = [];
          function resetData(){
            scope.dataHelp = [];
          }

          scope.$watch('ngModel',function(){
            resetData();
            if(scope.ngModel){
                var index = scope.data.indexOf(scope.ngModel);
                if(index > -1){
                  if(!scope.dataHelp[index]){
                    scope.dataHelp[index] = {};
                  }
                  scope.dataHelp[index].isChecked = true;
                }
            }
          });

          scope.selectionChanged = function($event,item){
           /* if(scope.ngModel === item){
              scope.ngModel = null;
            }  else {
              scope.ngModel = item;
            }*/
          }          
          
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
