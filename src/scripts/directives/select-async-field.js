'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtSelectAsyncField';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtSelectAsyncField.html',
        require: [],
        scope: {
          ngDisabled:'=',
          ngModel:'=',
          readMode:'=?',
          hasError:'=?',
          errorMessage:'@',
          errorPlacement:'@',
          label:'@?',
          dataPromise:'=data',
          ngModelProperty:"@",
          options:'@',
          colClass: '=?',
          rowClass: '@?',
          name:'@',
          ngModelBased:'@?',
          defaultOption:'@?'
        },
        link: function (scope, element, attrs, controller) {

          scope.data = [];

          scope.isLoading = true;
          scope.$watch('dataPromise', function() {
            scope.isLoading = true;
            scope.dataPromise.then(function(options) {
              scope.data = options;
              scope.isLoading = false;
            });
          });


          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithProperty  = function(){
            if(scope.ngModel){
              var item = scope.ngModel;
              if(scope.ngModelBased) {
                item = _.find(scope.data,function(o){
                  return o[scope.ngModelBased] === scope.ngModel;
                });
              }
              return item ? item[scope.ngModelProperty] : '-'
            }
            return "-";
          }

          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
        }
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
