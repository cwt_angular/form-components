'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtInputfieldText';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtInputfield.html',
        require: [],
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@?',
           name: '@',
           colClass: '=?',
           rowClass: '@?',
           ngModelPropertie:"@",
           ngModelBased:'@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }
                if (!item) {
                  return null;
                }
                return item[scope.ngModelPropertie]
                }

                return "";
            }
          
        }        
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
