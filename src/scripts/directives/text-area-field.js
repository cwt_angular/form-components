'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtTextAreaField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtTextAreaField.html',
        require: [],
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@',
           colClass: '=?',
           rowClass: '@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
