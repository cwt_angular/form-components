'use strict';
(function (module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
  var directiveName = 'cwtInflectionField';
  var theDirective = function ($filter) {
    return {
      restrict: 'E',
      templateUrl: 'templates/cwtInflectionField.html',
      require: ['ngModel', '^?form'],
      scope: {
        ngModel: '=',
        readMode: '=?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        rebateType: '@',
        fromType: '@',
        targetAddon: '@?',
        rebateAddon: '@?'
      },
      link: function (scope, element, attrs, controller) {
        scope.index = 0;
        scope.selectedRowId = 0;

        function setDirty() {
          if (controller[1]) {
            controller[1].$setDirty();
          }
        }

        scope.selectRow = function (id) {
          scope.selectedRowId = id;
        }

        scope.remove = function (layer) {
          setDirty();
          return scope.ngModel.splice(scope.ngModel.indexOf(layer), 1);
        };

        scope.dataModel = function () {
          return { id: null, target: null, rebate: null };
        };

        scope.addModel = function () {
          var model = scope.dataModel();
          model.id = angular.copy(scope.index);
          scope.ngModel.push(model)
          scope.index++;
          setDirty();
        }

        scope.hasNew = function () {
          var isNew, emptyLayer;
          isNew = false;
          if (!scope.ngModel) {
            scope.ngModel = [];
          }
          scope.ngModel.forEach(function (layer) {
            emptyLayer = true;
            //setDirty();
            Object.keys(scope.dataModel()).forEach(function (key) {
              if (key !== 'id' && layer[key] !== null) emptyLayer = false;
            });
            if (emptyLayer) isNew = true;
          });

          return isNew;
        }
        scope.$watch('readMode', function (newValue, oldValue, scope) {
          if (newValue === true) {
            scope.ngModel = $filter('orderBy')(scope.ngModel, scope.sorterMethod);
          }
        });
        scope.$watch('ngModel', function (newValue, oldValue, scope) {
          if (scope.readMode === true && JSON.stringify(newValue) !== JSON.stringify(oldValue) ) {
            scope.ngModel = $filter('orderBy')(newValue, scope.sorterMethod);
          }
        });
        scope.sorterMethod = function (data) {
          if (data.target) {
            return Number(data.target);
          }
          return data.target;
        }
        if (scope.ngModel.length <= 0) {
          scope.addModel();
        }
      }
    };
  };
  theDirective.$inject = ['$filter'];
  module.directive(directiveName, theDirective);
})(null);
