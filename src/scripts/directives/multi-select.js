'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtMultiSelectForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtMultiSelectFrom.html',
        require: [],
        scope: {
            ngDisabled: '=',
            ngModel: '=',
            readMode: '=?',
            hideSelectAll:'=?',
            hasError: '=?',
            errorMessage: '@',
            errorPlacement: '@',
            placeholder: '@',
            label: '@?',
            name: '@',
            colClass: '=?',
            displayLocation: '@?',
            rowClass: '@?',
            ngModelPropertie:"@",
            ngModelBased:'@?',
            showSearchbar:'@?',
            displayProperty:'@?',
            isLoading:'=?',
            emptyText:'@?',
            source:'=',
            selectAllLabel:'@?',
            allSelected:'=?',
            selectAllMessage:'@?',
            isflat:'@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }
                if (!item) {
                  return null;
                }
                return item[scope.ngModelPropertie]
                }

                return "";
            }

            function specialFn(event,obj){
              var target = $(event.target);
              var el = $(element.find('cwt-multi-select'));
              if (el.has(target).length === 0 && $(el[0]).isolateScope()) {
                $(el[0]).isolateScope().ctrlMultiSelect.clickHandler(event);
              }
            }

            var form = $(element).closest('cwt-form-wrapper');

            if (form && form.length > 0 && angular.element(form).scope()){
              var ctrl = angular.element(form).scope().controller;
              if(ctrl && ctrl.addSpecialLiseners){
                ctrl.addSpecialLiseners(specialFn);
              }              
            }          
        }        
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
