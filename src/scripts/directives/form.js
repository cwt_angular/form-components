'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtForm.html',
        require: ['^cwtFormWrapper'],
        transclude: true,
        replace: true,
        scope: {
           readMode:'=',
           ngModel:'=?',
           callbackFunction: '=',
           cancelFn: '&',
           name:'@',
           notActive:'=',
           isLoading:'=',
           values: '=',
           required:'=?',
           setPristine:'=?'
        },
        link: function (scope, element, attrs, controller) {
          scope.exposeController = controller[0];
          controller[0].registerForm(scope);
          scope.originalObject = {};
          scope.$watch('ngModel',function(){
            scope.originalObject.newest = angular.copy(scope.ngModel);
            scope.originalObject.init = angular.copy(scope.ngModel);
          });
          scope.$on("$destroy", function(){
            controller[0].unregisterForm(scope);
          });

          scope.$watch('notActive',function(value,oldVal) {
            if (value) {
              element.find('select').attr("disabled","disabled");
            } else {
              element.find('select').removeAttr("disabled");
            }
          })
        },
        controllerAs:'frmCtrl',
        controller:['$scope', '$window', '$timeout', '$element',function($scope, $window, $timeout, $element){
            $scope.index = -1;
            var ctrl = this;
            function broadcastReadOnly(isReadOnly, apply){
              $scope.readMode = isReadOnly;
              $scope.$broadcast('putInReadMode', { readMode: isReadOnly, apply: apply });
              if(isReadOnly){
                $scope.exposeController.noActiveForm($scope);
              }
            }

            function isCWTelement(element){
              if(element.nodeName.toLowerCase().indexOf("cwt") !== -1){
                return true;
              }
              else if(element.parentElement !== null){
                return isCWTelement(element.parentElement);
              }
            };

            $element[0].addEventListener('click', function (evt) {
              $scope.$apply(function(){
                ctrl.formclick(evt);
              });
            }, true);

            this.formclick = function(event){
              $scope.exposeController.formClick(event,{readMode:$scope.readMode,notActive:$scope.notActive});
              if ($scope.notActive || $scope.isLoading) {
                event.preventDefault();
                event.stopImmediatePropagation();
                event.stopPropagation();
                return false;
              }
              var target = $(event.target);
              if(target.hasClass('fa') || !$scope.readMode || (target.scope && target.scope() && target.scope().notActiveInForm) ||  target.closest('div').hasClass('form-footer')){
                return true;
              }
                //$scope.exposeController.clickHandle(event);

                if(!hasOtherFormsErrors()){
                  broadcastReadOnly(!$scope.readMode);
                } else {
                  $scope.exposeController.wait($scope);
                }
            }


            $scope.$on("$destroy",function(){
              $($element).off('click');
              $window.removeEventListener("click", haveClickedOutsideForm);
            });

            $scope.isValid = function(){
              return !($scope.originalObject.errorModel || $scope.saving || $scope[$scope.name].$dirty);
            }

            function hasOtherFormsErrors(){
              if($scope.exposeController){
                if($scope.exposeController.hasAFormErrors()){
                  return true;
                }
              }
              return false;
            }

            function checkForErrors() {

            }

            function haveClickedOutsideForm(event){
              checkForErrors();
              var ClickedForm = $(event.target).closest('form');
              if(ClickedForm && ClickedForm.scope()){
                ClickedForm = ClickedForm.scope().name;
              } else {
                ClickedForm = 'outside';
              }
              var save = false;
              if (ClickedForm !== $scope.name) {
                if ($scope[$scope.name].$dirty) {
                  if (!$scope.readMode) {
                    $scope.saveChanges();
                  }
                }else{
                  $scope.cancelForm();
                }
                   /*//if($scope[$scope.name].$dirty){
                    if(!_.isEqual(angular.copy($scope.ngModel), $scope.originalObject.newest)){
                     if(!$scope.saving && !$scope.readMode){
                        save = true;
                        $scope.saveChanges();
                     }
                    }
                   /*} else {
                     if(!hasOtherFormsErrors()){
                       $scope.cancelFn();
                     }
                   }**/

                 /*if(!save && !$scope.originalObject.errorModel && !hasOtherFormsErrors()){
                   broadcastReadOnly(true);
                 }*/
              }
            };

            $scope.$watch('isLoading',function(val,oldVal){
              if(val !== oldVal){
                $scope.saving = val;
              }
            });

             $scope.$watch('setPristine',function(val){
              if(val !== undefined){
                 $scope[$scope.name].$setPristine();
              }
            });

            function savingChanges(){
              if(!$scope.saving){
                  $scope.saving = true;
                  $scope.callbackFunction().then(function(){
                      $scope.originalObject.newest = angular.copy($scope.ngModel);
                      $scope.originalObject.errorModel = null;
                      $scope[$scope.name].$setPristine();
                      $scope.exposeController.noActiveForm($scope);
                      broadcastReadOnly(true);
                    },function(errors) {
                      $scope.originalObject.errorModel = angular.copy($scope.ngModel);
                      $scope.$broadcast('adressErrors', errors);
                    }).finally(function(){
                      $scope.saving = false;
                    });
              }
            }

            $scope.saveChanges = function(){
              if(!isEqual(angular.copy($scope.ngModel), $scope.originalObject.newest) && !isEqual(angular.copy($scope.ngModel), $scope.originalObject.errorModel)){
                savingChanges();
              }
              else if(isEqual(angular.copy($scope.ngModel), $scope.originalObject.errorModel)){
                //
              }
              else{
                broadcastReadOnly(true);
              }
            };
            var internChange = false;
            $scope.cancelForm = function(){
              $scope.readMode = internChange = true;
              $scope.resetForm();
              $scope.exposeController.noActiveForm($scope);
              $timeout(function(){
                $scope.cancelFn();
              },0);
            }

            function copy(dest,source){
              try {
                if(typeof source == "object"){
                  $scope.values.forEach(function(f){
                    if (angular.isArray(source[f])) {
                      var arrayClone = [];
                      source[f].forEach(function(item) {
                        arrayClone.push(angular.copy(item));
                      });
                      dest[f] = arrayClone;
                    } else {
                      dest[f] = source[f];
                    }
                  });
                }
              } catch(e){

              }
            }

            function isEqual(dest,source){
              var equal = true;
              try {
                if(typeof source == "object"){
                  $scope.values.forEach(function(f){
                    if(dest[f] !== source[f]){
                      equal = false
                    }
                  });
                } else {
                  equal = false;
                }
              } catch(e){
                equal = false;
              }
              return equal;
            }

            $scope.resetForm = function(){

              if($scope.originalObject.newest !== undefined){
                copy($scope.ngModel,$scope.originalObject.newest);
              }
              else{
                copy($scope.ngModel,$scope.originalObject.init);
                copy($scope.originalObject.newest, $scope.ngModel);
              }
              $scope.originalObject.errorModel = null;
              $scope[$scope.name].$setPristine();
            }

            function init(){
            };

            //$window.addEventListener("click", haveClickedOutsideForm);

             $scope.$watch('readMode',function(newVal,oldVal){
               var classElement =  $('body');
               if ($scope.readMode) {
                  if (!internChange) {
                    $scope.resetForm();
                 }
                 $scope[$scope.name].$setPristine();
                  //classElement.removeClass('has-bottom-sticky');
               } else {
                $timeout(function(){
                  $scope.exposeController.setActiveForm($scope);
                });
                //classElement.addClass('has-bottom-sticky');
               }
               if(newVal !== oldVal){
                 broadcastReadOnly($scope.readMode);
               }
               internChange = false;
             });

             $scope.$on("$destroy", function(){
               $window.removeEventListener("click", haveClickedOutsideForm);
             });

             this.changeReadMode = function(value){
               $scope.readMode = value;
             }

             this.getReadModeValue = function(){
               return $scope.readMode;
             }

             init();

        }]
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
