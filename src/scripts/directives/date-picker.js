'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtDatePickerForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtDatepicker.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            outputModel:'=?',
            readMode:'=?',
            hasError:'=?',
            errorMessage:'@?',
            errorPlacement:'@?',
            label:'@?',
            colClass:'=?',
            rowClass:'=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-12',
              label: 'col-md-12'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }        
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
