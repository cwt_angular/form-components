'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtNumberFieldText';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        require:'ngModel',
        templateUrl: 'templates/cwtNumberInputfield.html',
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@?',
           name: '@',
           colClass: '=?',
           rowClass: '@?',
           ngModelPropertie:"@",
           ngModelBased:'@?',
           addon:'@?',
           focusFn: '&?',
           paddingLeft: '=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          if (!scope.addon) {
            scope.addon = '';
          }

          scope.showAddon = function(){
            if(scope.addon && scope.addon.length > 0){
              return true;
            }
            return false;
          }

          controller.$formatters.push(function(value) {
            var parsed = parseFloat(value);
            if(isNaN(parsed)){
              return undefined;
            }
            return parsed;
          });

          scope.ngChangefire = function(){
            if (attrs.ngChange) {
              var attrCopy = attrs.ngChange;
              $timeout(function(){
                scope.$parent.$eval(attrCopy);
              },0);
            }
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }              
                return item[scope.ngModelPropertie]
                }

                return "";
            }
          
        }        
    };
  };
  theDirective.$inject = ["$timeout"];
  module.directive(directiveName, theDirective);
})(null);
