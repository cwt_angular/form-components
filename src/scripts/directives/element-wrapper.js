'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtElementWrapper';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtFormElementWrapper.html',
        transclude:true,
        require:'^cwtForm',
        scope:true,
        link: function (scope, element, attrs, controller) {
            function init(){
                scope.wrapperReadMode = controller.getReadModeValue();
            }
            init();
            function getCwtElement(){
                return element.find("*").filter(function(){
                    return /^cwt\-/i.test(this.nodeName);
                });
            }
            scope.$watch('wrapperReadMode',function(){

                $timeout(function() {
                    controller.changeReadMode(scope.wrapperReadMode);
                    if(getCwtElement() && getCwtElement().isolateScope() && !getCwtElement().attr('read-mode')){
                        getCwtElement().isolateScope().readMode = scope.wrapperReadMode;
                    }                    
                }, 0);

            });

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            scope.$on('putInReadMode', function(event, args) {
                resetForm();
                scope.safeApply(function(){
                    scope.wrapperReadMode = args.readMode;
                });
            });

            function resetForm(){
                if(getCwtElement() && getCwtElement().isolateScope()){
                    getCwtElement().isolateScope().hasError = false;
                    getCwtElement().isolateScope().errorMessage = '';
                }
            }

            scope.$on('adressErrors', function(event, args) {
                resetForm();
                
                var errorObject;
                var formElement = getCwtElement();
                if(args){
                    errorObject = _.find(args.fields, function(obj) { return obj.property === formElement.attr('field-name') })
                }
                if(errorObject != undefined)
                {
                    formElement.isolateScope().hasError = true;
                    formElement.isolateScope().errorMessage = errorObject.message;
                    if(formElement.isolateScope().errorPlacement === undefined){
                        formElement.isolateScope().errorPlacement = 'bottom';
                    }
                }
            });
        },
        controller:['$scope',function($scope){
        }]
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);