'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtTypeahead';
    var theDirective = function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/cwtTypeahead.html',
          require: [],
          scope: {
             ngDisabled: '=',
             ngModel: '=',
             readMode: '=?',
             hasError: '=?',
             errorMessage: '@',
             errorPlacement: '@',
             placeholder: '@',
             label: '@?',
             name: '@',
             colClass: '=?',
             rowClass: '@?',
             typeaheadWaitMs:"@",
             typeaheadMinLength:"@",
             uibTypeahead:"@",
             typeaheadPromise:"&",
             data:"=",
             ngModelPropertie:"@",
             ngModelBased:'@?'
          },
          compile: function(tElem,tAttr){
              $(tElem).find('input').attr('uib-typeahead',tAttr.uibTypeahead);
              $(tElem).find('input').attr('typeahead-min-length',tAttr.typeaheadMinLength);
              $(tElem).find('input').attr('typeahead-wait-ms',tAttr.typeaheadWaitMs);
              return {
                  post:function (scope, element, attrs, controller) {
                      element.find('input').attr('uib-typeahead',scope.uibTypeahead);
                      if(!scope.colClass){
                          scope.colClass = {
                          field: 'col-md-8',
                          label: 'col-md-4'
                          }
                      }
                      if(!scope.rowClass){
                          scope.rowClass = 'form-group row';
                      }

                      scope.uibTypeaheadPromise = function(val){
                        return scope.typeaheadPromise({$viewValue:val});
                      }

                      scope.ngModelWithPropertie  = function(){
                          if(scope.ngModel){
                          var item = scope.ngModel;
                          if(scope.ngModelBased) {
                              item = _.find(scope.data,function(o){
                              return o[scope.ngModelBased] === scope.ngModel;
                              });
                          }              
                          return item[scope.ngModelPropertie]
                          }

                          return "";
                      }
                  }
              }
          }
        }
    };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
  module.filter('sortTypeAhead', function () {
    return function (data, input, sortFirst, sortSecond) {
      var first = [];
      var others = [];
      if (sortFirst && data && data[0] && data[0][sortFirst]) {
        for (var i = 0; i < data.length; i++) {
          if (data[i][sortFirst].toLowerCase().indexOf(input.toLowerCase()) == 0) {
              first.push(data[i]);
          } else {
              others.push(data[i]);
          }
        }
        first = _.sortBy(first, function(o){return o[sortFirst]});
        others = sortSecond ? _.sortBy(others, function(o){return o[sortSecond]}) : others;
        return (first.concat(others));
      } else {
        return data;
      }
      
    }
  });
})(null);
