'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtCheckboxesField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtCheckboxesfield.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            readMode:'=?',
            label:'@',
            hasError:'=?',
            errorMessage:'@',
            errorPlacement:'@',
            ngClick:'&',
            isLoading:'=',
            data:'=',
            colClass: '=?',
            rowClass: '@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithPropertie  = function(){
            if(scope.ngModel){
              return scope.ngModel[scope.ngModelPropertie]
            }
            return "";
          }

          scope.dataHelp = [];

          if(!Array.isArray(scope.ngModel)){
            scope.ngModel = [];
          }

          function resetData(){
            scope.dataHelp.forEach(function(item,index){
              item.isChecked = false;
            })
          }

          scope.$watch('ngModel',function(){
            resetData();
            if(scope.ngModel){
              scope.ngModel.forEach(function(item){
                var index = scope.data.indexOf(item);
                if(index > -1){
                  if(!scope.dataHelp[index]){
                    scope.dataHelp[index] = {};
                  }
                  scope.dataHelp[index].isChecked = true;
                }
              });
            }
           
          },true);

          scope.selectionChanged = function($event,item){
           // $event.preventDefault();
            var index = scope.ngModel.indexOf(item);
            if(scope.ngModel &&  index> -1){
              scope.ngModel.splice(index,1);
            }else{
              scope.ngModel.push(item);
            }
            scope.ngClick({$event:event,item:item});
          }
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
