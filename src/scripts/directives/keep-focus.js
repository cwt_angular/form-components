'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }

    var directiveName = 'keepFocus';
    var focussed = null;
    var theDirective = function ($timeout) {
      return {
        restrict: 'A',
        link: function ($scope, $element, attrs) {
          attrs.$observe('index', function (val) {
              if(attrs.rowId == attrs.selectedRowId)
              {
                  $timeout(function(){
                      $element[0].querySelector('input').focus();
                  });
              }
          });
        }
      };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
