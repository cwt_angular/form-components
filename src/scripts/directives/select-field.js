'use strict';
(function (module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
  var directiveName = 'cwtSelectField';
  var theDirective = function ($timeout) {
    return {
      restrict: 'E',
      templateUrl: 'templates/cwtSelectfield.html',
      require: [],
      scope: {
        ngDisabled: '=',
        ngModel: '=',
        readMode: '=?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        label: '@?',
        isLoading: '=',
        data: '=',
        ngModelPropertie: "@",
        options: '@',
        optionsFilter: '<?',
        colClass: '=?',
        rowClass: '@?',
        name: '@',
        notActiveInForm: '@?',
        ngModelBased: '@?',
        defaultOption: '@?',
        ngChange: '&?',
        changeFn: '=?'
      },
      link: function (scope, element, attrs, controller) {
        //owner.name for owner in ngModel track by owner.id
        scope.ngModelWithPropertie = function () {
          if (scope.ngModel) {
            var item = scope.ngModel;
            if (scope.ngModelBased) {
              item = _.find(scope.data, function (o) {
                return o[scope.ngModelBased] === scope.ngModel;
              });
            }
            if (!item) {
              return null;
            }
            return item[scope.ngModelPropertie]
          }

          return "";
        }

        scope.change = function (obj) {
          if (scope.ngChange) {
            scope.ngChange({ data: obj });
          }
          if (scope.changeFn) {
            scope.changeFn(obj);
          }
        }

        if (!scope.colClass) {
          scope.colClass = {
            field: 'col-md-8',
            label: 'col-md-4'
          }
        }
        if (!scope.rowClass) {
          scope.rowClass = 'form-group row';
        }
      }
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
