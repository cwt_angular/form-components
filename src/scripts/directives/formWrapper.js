'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtFormWrapper';
    var theDirective = function ($window,$uibModal) {
        return {
        restrict: 'E',
        require:'cwtFormWrapper',
        scope:true,
        link: function (scope, element, attrs, controller) {
                  
        },
        controller:['$scope',function($scope){
            var ctrl = this;
            $scope.controller = this;
            ctrl.register = [];
            ctrl.specialLiseners=[];
            var activeForm = null;
            var waitForm = null;
            ctrl.setActiveForm = function(form) {
              if (activeForm) {
                activeForm.cancelForm();
              }
              activeForm = form;
              $('body').addClass('has-bottom-sticky');
            }

            ctrl.noActiveForm = function(){
              activeForm = null;
              $('body').removeClass('has-bottom-sticky');
            }
            ctrl.getActiveForm = function() {
              return activeForm;
            }
            ctrl.activeFormHasErrors = function (){
              if(activeForm && activeForm.hasError()){
                return true;
              }
              return false;
            }
            ctrl.wait = function (form){
              waitForm = form;
            }
            ctrl.registerForm = function(form){
                ctrl.register.push(form);
            }
            ctrl.unregisterForm = function(form){
              var index = ctrl.register.indexOf(form);
              ctrl.register.splice(index,1);
            }
            ctrl.hasAFormErrors = function() {
                var hasError = false;
                _.each(ctrl.register,function(o){
                    if(!o.isValid()){
                        hasError = true;
                    }
                });
                return hasError;
            }

            ctrl.formClick = function(event,obj) {
              if (obj.notAcitve) {
                return false;
              }
              for (var i = 0, len = ctrl.specialLiseners.length; i < len; i++) {
                ctrl.specialLiseners[i](event,obj);
              }
            };

            ctrl.addSpecialLiseners = function(fn) {
              ctrl.specialLiseners.push(fn);
            };

            $scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            

            function haveClickedOutsideForm(event){
              $scope.safeApply(function(){
                var active = ctrl.getActiveForm();
                var ClickedForm;
                if(event){
                  ClickedForm = $(event.target).closest('form');
                }
                 
                if (ClickedForm && ClickedForm.scope()) {
                  ClickedForm = ClickedForm.scope().name;
                } else {
                  ClickedForm = 'outside';
                }

                if(active && active.name !== ClickedForm){                
                  if(active[active.name].$dirty){
                    if (!active.readMode) {

                      var modal = $uibModal.open({
                        animation: true,
                        templateUrl:'templates/modal-save.html',
                        size: 'sm',
                        scope: $scope
                      });
                      modal.result.then(function(){
                        
                      },function(){
                        active.cancelForm();
                        waitForm.frmCtrl.formclick(event);
                        waitForm = null;
                      });
                    }
                  }else{
                    active.cancelForm();
                  }
                }
              });        
            };
            ctrl.clickHandle = haveClickedOutsideForm;
            //$window.addEventListener("click", haveClickedOutsideForm);
        }]
    };
  };
  theDirective.$inject = ["$window","$uibModal"];
  module.directive(directiveName, theDirective);
})(null);