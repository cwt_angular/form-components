'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'restrictInput';
    var theDirective = function () {
        return {
        restrict: 'A',
        priority:999,
        require: [],
        link: function (scope, element, attrs, controller) {
            var ele = $(element[0]);
            var regex = /(^[0-9]{1,14})(\.[0-9]{1,4})?$/i;

            String.prototype.insertAt = function(index, string) { 
                return this.substr(0, index) + string + this.substr(index);
            }

            function remove(str, startIndex, count) {
                return str.substr(0, startIndex) + str.substr(startIndex + count);
            }

            function getCaretPos(input) {

                var caret = {
                    start: -1,
                    stop: -1
                }

                // Internet Explorer Caret Position (TextArea)
                if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    var bookmark = range.getBookmark();
                    caret.start = bookmark.charCodeAt(2) - 2;
                    caret.stop = input.selectionEnd;
                } else {
                    // Firefox Caret Position (TextArea)
                    if (input.setSelectionRange)
                         caret.start = input.selectionStart;
                         caret.stop = input.selectionEnd;
                }

                return caret;
            }

            if (ele.type !== "number") {
                ele.bind('keypress',function(e){
                    var charCode = String.fromCharCode(e.keyCode || e.which);
                    var key = e.key || charCode;
                    if(!key) {
                        return;
                    }
                    var start = getCaretPos($(this)[0]).start;
                    var stop = getCaretPos($(this)[0]).stop;
                    var value = $(this).val();
                    if(start !== stop) {
                        value = remove(value, start, (stop - start));
                    }
                    value = value.insertAt(start,key);
                    if(key === '.'){
                        if ((value.match(new RegExp("[.]", "g")) || []).length > 1) {
                        e.preventDefault(); 
                        }
                    } else if (!RegExp(regex).test(value)){
                        e.preventDefault();
                    }
                });
            }

            scope.$on("$destroy", function(){
               ele.unbind('keypress');
            });   
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
