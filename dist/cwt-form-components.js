'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtCheckboxField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtCheckboxfield.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            readMode:'=?',
            label:'@',
            hasError:'=?',
            id:'@?',
            errorMessage:'@',
            errorPlacement:'@',
            ngClick:'&',
            isLoading:'=',
            colClass: '=?',
            rowClass: '@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
              scope.colClass = {
                field: 'col-md-8',
                label: 'col-md-4'
              }
            }
            if(!scope.rowClass){
              scope.rowClass = 'form-group row';
            }
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtCheckboxesField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtCheckboxesfield.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            readMode:'=?',
            label:'@',
            hasError:'=?',
            errorMessage:'@',
            errorPlacement:'@',
            ngClick:'&',
            isLoading:'=',
            data:'=',
            colClass: '=?',
            rowClass: '@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithPropertie  = function(){
            if(scope.ngModel){
              return scope.ngModel[scope.ngModelPropertie]
            }
            return "";
          }

          scope.dataHelp = [];

          if(!Array.isArray(scope.ngModel)){
            scope.ngModel = [];
          }

          function resetData(){
            scope.dataHelp.forEach(function(item,index){
              item.isChecked = false;
            })
          }

          scope.$watch('ngModel',function(){
            resetData();
            if(scope.ngModel){
              scope.ngModel.forEach(function(item){
                var index = scope.data.indexOf(item);
                if(index > -1){
                  if(!scope.dataHelp[index]){
                    scope.dataHelp[index] = {};
                  }
                  scope.dataHelp[index].isChecked = true;
                }
              });
            }
           
          },true);

          scope.selectionChanged = function($event,item){
           // $event.preventDefault();
            var index = scope.ngModel.indexOf(item);
            if(scope.ngModel &&  index> -1){
              scope.ngModel.splice(index,1);
            }else{
              scope.ngModel.push(item);
            }
            scope.ngClick({$event:event,item:item});
          }
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtDatePickerForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtDatepicker.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            outputModel:'=?',
            readMode:'=?',
            hasError:'=?',
            errorMessage:'@?',
            errorPlacement:'@?',
            label:'@?',
            colClass:'=?',
            rowClass:'=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-12',
              label: 'col-md-12'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }        
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtElementWrapper';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtFormElementWrapper.html',
        transclude:true,
        require:'^cwtForm',
        scope:true,
        link: function (scope, element, attrs, controller) {
            function init(){
                scope.wrapperReadMode = controller.getReadModeValue();
            }
            init();
            function getCwtElement(){
                return element.find("*").filter(function(){
                    return /^cwt\-/i.test(this.nodeName);
                });
            }
            scope.$watch('wrapperReadMode',function(){

                $timeout(function() {
                    controller.changeReadMode(scope.wrapperReadMode);
                    if(getCwtElement() && getCwtElement().isolateScope() && !getCwtElement().attr('read-mode')){
                        getCwtElement().isolateScope().readMode = scope.wrapperReadMode;
                    }                    
                }, 0);

            });

            scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            scope.$on('putInReadMode', function(event, args) {
                resetForm();
                scope.safeApply(function(){
                    scope.wrapperReadMode = args.readMode;
                });
            });

            function resetForm(){
                if(getCwtElement() && getCwtElement().isolateScope()){
                    getCwtElement().isolateScope().hasError = false;
                    getCwtElement().isolateScope().errorMessage = '';
                }
            }

            scope.$on('adressErrors', function(event, args) {
                resetForm();
                
                var errorObject;
                var formElement = getCwtElement();
                if(args){
                    errorObject = _.find(args.fields, function(obj) { return obj.property === formElement.attr('field-name') })
                }
                if(errorObject != undefined)
                {
                    formElement.isolateScope().hasError = true;
                    formElement.isolateScope().errorMessage = errorObject.message;
                    if(formElement.isolateScope().errorPlacement === undefined){
                        formElement.isolateScope().errorPlacement = 'bottom';
                    }
                }
            });
        },
        controller:['$scope',function($scope){
        }]
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtForm.html',
        require: ['^cwtFormWrapper'],
        transclude: true,
        replace: true,
        scope: {
           readMode:'=',
           ngModel:'=?',
           callbackFunction: '=',
           cancelFn: '&',
           name:'@',
           notActive:'=',
           isLoading:'=',
           values: '=',
           required:'=?',
           setPristine:'=?'
        },
        link: function (scope, element, attrs, controller) {
          scope.exposeController = controller[0];
          controller[0].registerForm(scope);
          scope.originalObject = {};
          scope.$watch('ngModel',function(){
            scope.originalObject.newest = angular.copy(scope.ngModel);
            scope.originalObject.init = angular.copy(scope.ngModel);
          });
          scope.$on("$destroy", function(){
            controller[0].unregisterForm(scope);
          });

          scope.$watch('notActive',function(value,oldVal) {
            if (value) {
              element.find('select').attr("disabled","disabled");
            } else {
              element.find('select').removeAttr("disabled");
            }
          })
        },
        controllerAs:'frmCtrl',
        controller:['$scope', '$window', '$timeout', '$element',function($scope, $window, $timeout, $element){
            $scope.index = -1;
            var ctrl = this;
            function broadcastReadOnly(isReadOnly, apply){
              $scope.readMode = isReadOnly;
              $scope.$broadcast('putInReadMode', { readMode: isReadOnly, apply: apply });
              if(isReadOnly){
                $scope.exposeController.noActiveForm($scope);
              }
            }

            function isCWTelement(element){
              if(element.nodeName.toLowerCase().indexOf("cwt") !== -1){
                return true;
              }
              else if(element.parentElement !== null){
                return isCWTelement(element.parentElement);
              }
            };

            $element[0].addEventListener('click', function (evt) {
              $scope.$apply(function(){
                ctrl.formclick(evt);
              });
            }, true);

            this.formclick = function(event){
              $scope.exposeController.formClick(event,{readMode:$scope.readMode,notActive:$scope.notActive});
              if ($scope.notActive || $scope.isLoading) {
                event.preventDefault();
                event.stopImmediatePropagation();
                event.stopPropagation();
                return false;
              }
              var target = $(event.target);
              if(target.hasClass('fa') || !$scope.readMode || (target.scope && target.scope() && target.scope().notActiveInForm) ||  target.closest('div').hasClass('form-footer')){
                return true;
              }
                //$scope.exposeController.clickHandle(event);

                if(!hasOtherFormsErrors()){
                  broadcastReadOnly(!$scope.readMode);
                } else {
                  $scope.exposeController.wait($scope);
                }
            }


            $scope.$on("$destroy",function(){
              $($element).off('click');
              $window.removeEventListener("click", haveClickedOutsideForm);
            });

            $scope.isValid = function(){
              return !($scope.originalObject.errorModel || $scope.saving || $scope[$scope.name].$dirty);
            }

            function hasOtherFormsErrors(){
              if($scope.exposeController){
                if($scope.exposeController.hasAFormErrors()){
                  return true;
                }
              }
              return false;
            }

            function checkForErrors() {

            }

            function haveClickedOutsideForm(event){
              checkForErrors();
              var ClickedForm = $(event.target).closest('form');
              if(ClickedForm && ClickedForm.scope()){
                ClickedForm = ClickedForm.scope().name;
              } else {
                ClickedForm = 'outside';
              }
              var save = false;
              if (ClickedForm !== $scope.name) {
                if ($scope[$scope.name].$dirty) {
                  if (!$scope.readMode) {
                    $scope.saveChanges();
                  }
                }else{
                  $scope.cancelForm();
                }
                   /*//if($scope[$scope.name].$dirty){
                    if(!_.isEqual(angular.copy($scope.ngModel), $scope.originalObject.newest)){
                     if(!$scope.saving && !$scope.readMode){
                        save = true;
                        $scope.saveChanges();
                     }
                    }
                   /*} else {
                     if(!hasOtherFormsErrors()){
                       $scope.cancelFn();
                     }
                   }**/

                 /*if(!save && !$scope.originalObject.errorModel && !hasOtherFormsErrors()){
                   broadcastReadOnly(true);
                 }*/
              }
            };

            $scope.$watch('isLoading',function(val,oldVal){
              if(val !== oldVal){
                $scope.saving = val;
              }
            });

             $scope.$watch('setPristine',function(val){
              if(val !== undefined){
                 $scope[$scope.name].$setPristine();
              }
            });

            function savingChanges(){
              if(!$scope.saving){
                  $scope.saving = true;
                  $scope.callbackFunction().then(function(){
                      $scope.originalObject.newest = angular.copy($scope.ngModel);
                      $scope.originalObject.errorModel = null;
                      $scope[$scope.name].$setPristine();
                      $scope.exposeController.noActiveForm($scope);
                      broadcastReadOnly(true);
                    },function(errors) {
                      $scope.originalObject.errorModel = angular.copy($scope.ngModel);
                      $scope.$broadcast('adressErrors', errors);
                    }).finally(function(){
                      $scope.saving = false;
                    });
              }
            }

            $scope.saveChanges = function(){
              if(!isEqual(angular.copy($scope.ngModel), $scope.originalObject.newest) && !isEqual(angular.copy($scope.ngModel), $scope.originalObject.errorModel)){
                savingChanges();
              }
              else if(isEqual(angular.copy($scope.ngModel), $scope.originalObject.errorModel)){
                //
              }
              else{
                broadcastReadOnly(true);
              }
            };
            var internChange = false;
            $scope.cancelForm = function(){
              $scope.readMode = internChange = true;
              $scope.resetForm();
              $scope.exposeController.noActiveForm($scope);
              $timeout(function(){
                $scope.cancelFn();
              },0);
            }

            function copy(dest,source){
              try {
                if(typeof source == "object"){
                  $scope.values.forEach(function(f){
                    if (angular.isArray(source[f])) {
                      var arrayClone = [];
                      source[f].forEach(function(item) {
                        arrayClone.push(angular.copy(item));
                      });
                      dest[f] = arrayClone;
                    } else {
                      dest[f] = source[f];
                    }
                  });
                }
              } catch(e){

              }
            }

            function isEqual(dest,source){
              var equal = true;
              try {
                if(typeof source == "object"){
                  $scope.values.forEach(function(f){
                    if(dest[f] !== source[f]){
                      equal = false
                    }
                  });
                } else {
                  equal = false;
                }
              } catch(e){
                equal = false;
              }
              return equal;
            }

            $scope.resetForm = function(){

              if($scope.originalObject.newest !== undefined){
                copy($scope.ngModel,$scope.originalObject.newest);
              }
              else{
                copy($scope.ngModel,$scope.originalObject.init);
                copy($scope.originalObject.newest, $scope.ngModel);
              }
              $scope.originalObject.errorModel = null;
              $scope[$scope.name].$setPristine();
            }

            function init(){
            };

            //$window.addEventListener("click", haveClickedOutsideForm);

             $scope.$watch('readMode',function(newVal,oldVal){
               var classElement =  $('body');
               if ($scope.readMode) {
                  if (!internChange) {
                    $scope.resetForm();
                 }
                 $scope[$scope.name].$setPristine();
                  //classElement.removeClass('has-bottom-sticky');
               } else {
                $timeout(function(){
                  $scope.exposeController.setActiveForm($scope);
                });
                //classElement.addClass('has-bottom-sticky');
               }
               if(newVal !== oldVal){
                 broadcastReadOnly($scope.readMode);
               }
               internChange = false;
             });

             $scope.$on("$destroy", function(){
               $window.removeEventListener("click", haveClickedOutsideForm);
             });

             this.changeReadMode = function(value){
               $scope.readMode = value;
             }

             this.getReadModeValue = function(){
               return $scope.readMode;
             }

             init();

        }]
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtFormWrapper';
    var theDirective = function ($window,$uibModal) {
        return {
        restrict: 'E',
        require:'cwtFormWrapper',
        scope:true,
        link: function (scope, element, attrs, controller) {
                  
        },
        controller:['$scope',function($scope){
            var ctrl = this;
            $scope.controller = this;
            ctrl.register = [];
            ctrl.specialLiseners=[];
            var activeForm = null;
            var waitForm = null;
            ctrl.setActiveForm = function(form) {
              if (activeForm) {
                activeForm.cancelForm();
              }
              activeForm = form;
              $('body').addClass('has-bottom-sticky');
            }

            ctrl.noActiveForm = function(){
              activeForm = null;
              $('body').removeClass('has-bottom-sticky');
            }
            ctrl.getActiveForm = function() {
              return activeForm;
            }
            ctrl.activeFormHasErrors = function (){
              if(activeForm && activeForm.hasError()){
                return true;
              }
              return false;
            }
            ctrl.wait = function (form){
              waitForm = form;
            }
            ctrl.registerForm = function(form){
                ctrl.register.push(form);
            }
            ctrl.unregisterForm = function(form){
              var index = ctrl.register.indexOf(form);
              ctrl.register.splice(index,1);
            }
            ctrl.hasAFormErrors = function() {
                var hasError = false;
                _.each(ctrl.register,function(o){
                    if(!o.isValid()){
                        hasError = true;
                    }
                });
                return hasError;
            }

            ctrl.formClick = function(event,obj) {
              if (obj.notAcitve) {
                return false;
              }
              for (var i = 0, len = ctrl.specialLiseners.length; i < len; i++) {
                ctrl.specialLiseners[i](event,obj);
              }
            };

            ctrl.addSpecialLiseners = function(fn) {
              ctrl.specialLiseners.push(fn);
            };

            $scope.safeApply = function(fn) {
              var phase = this.$root.$$phase;
              if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                  fn();
                }
              } else {
                this.$apply(fn);
              }
            };

            

            function haveClickedOutsideForm(event){
              $scope.safeApply(function(){
                var active = ctrl.getActiveForm();
                var ClickedForm;
                if(event){
                  ClickedForm = $(event.target).closest('form');
                }
                 
                if (ClickedForm && ClickedForm.scope()) {
                  ClickedForm = ClickedForm.scope().name;
                } else {
                  ClickedForm = 'outside';
                }

                if(active && active.name !== ClickedForm){                
                  if(active[active.name].$dirty){
                    if (!active.readMode) {

                      var modal = $uibModal.open({
                        animation: true,
                        templateUrl:'templates/modal-save.html',
                        size: 'sm',
                        scope: $scope
                      });
                      modal.result.then(function(){
                        
                      },function(){
                        active.cancelForm();
                        waitForm.frmCtrl.formclick(event);
                        waitForm = null;
                      });
                    }
                  }else{
                    active.cancelForm();
                  }
                }
              });        
            };
            ctrl.clickHandle = haveClickedOutsideForm;
            //$window.addEventListener("click", haveClickedOutsideForm);
        }]
    };
  };
  theDirective.$inject = ["$window","$uibModal"];
  module.directive(directiveName, theDirective);
})(null);;'use strict';
(function (module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
  var directiveName = 'cwtInflectionField';
  var theDirective = function ($filter) {
    return {
      restrict: 'E',
      templateUrl: 'templates/cwtInflectionField.html',
      require: ['ngModel', '^?form'],
      scope: {
        ngModel: '=',
        readMode: '=?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        rebateType: '@',
        fromType: '@',
        targetAddon: '@?',
        rebateAddon: '@?'
      },
      link: function (scope, element, attrs, controller) {
        scope.index = 0;
        scope.selectedRowId = 0;

        function setDirty() {
          if (controller[1]) {
            controller[1].$setDirty();
          }
        }

        scope.selectRow = function (id) {
          scope.selectedRowId = id;
        }

        scope.remove = function (layer) {
          setDirty();
          return scope.ngModel.splice(scope.ngModel.indexOf(layer), 1);
        };

        scope.dataModel = function () {
          return { id: null, target: null, rebate: null };
        };

        scope.addModel = function () {
          var model = scope.dataModel();
          model.id = angular.copy(scope.index);
          scope.ngModel.push(model)
          scope.index++;
          setDirty();
        }

        scope.hasNew = function () {
          var isNew, emptyLayer;
          isNew = false;
          if (!scope.ngModel) {
            scope.ngModel = [];
          }
          scope.ngModel.forEach(function (layer) {
            emptyLayer = true;
            //setDirty();
            Object.keys(scope.dataModel()).forEach(function (key) {
              if (key !== 'id' && layer[key] !== null) emptyLayer = false;
            });
            if (emptyLayer) isNew = true;
          });

          return isNew;
        }
        scope.$watch('readMode', function (newValue, oldValue, scope) {
          if (newValue === true) {
            scope.ngModel = $filter('orderBy')(scope.ngModel, scope.sorterMethod);
          }
        });
        scope.$watch('ngModel', function (newValue, oldValue, scope) {
          if (scope.readMode === true && JSON.stringify(newValue) !== JSON.stringify(oldValue) ) {
            scope.ngModel = $filter('orderBy')(newValue, scope.sorterMethod);
          }
        });
        scope.sorterMethod = function (data) {
          if (data.target) {
            return Number(data.target);
          }
          return data.target;
        }
        if (scope.ngModel.length <= 0) {
          scope.addModel();
        }
      }
    };
  };
  theDirective.$inject = ['$filter'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtInputfieldText';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtInputfield.html',
        require: [],
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@?',
           name: '@',
           colClass: '=?',
           rowClass: '@?',
           ngModelPropertie:"@",
           ngModelBased:'@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }
                if (!item) {
                  return null;
                }
                return item[scope.ngModelPropertie]
                }

                return "";
            }
          
        }        
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }

    var directiveName = 'keepFocus';
    var focussed = null;
    var theDirective = function ($timeout) {
      return {
        restrict: 'A',
        link: function ($scope, $element, attrs) {
          attrs.$observe('index', function (val) {
              if(attrs.rowId == attrs.selectedRowId)
              {
                  $timeout(function(){
                      $element[0].querySelector('input').focus();
                  });
              }
          });
        }
      };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function (module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
  var directiveName = 'cwtLayerField';
  var theDirective = function ($filter) {
    return {
      restrict: 'E',
      templateUrl: 'templates/cwtLayerField.html',
      require: ['ngModel', '^?form'],
      scope: {
        ngModel: '=',
        readMode: '=?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        rebateType: '@',
        fromType: '@',
        targetAddon: '@?',
        rebateAddon: '@?',
        targetLabel: '@?',
        rebateLabel: '@?'
      },
      link: function (scope, element, attrs, controller) {
        scope.index = 0;
        scope.selectedRowId = 0;

        function setDirty() {
          if (controller[1]) {
            controller[1].$setDirty();
          }
        }

        function setPristine(){
          if(controller[1]) {
            controller[1].$setPristine();
          }
        }

        scope.selectRow = function (id) {
          scope.selectedRowId = id;
        }

        scope.remove = function (layer) {
          setDirty();
          return scope.ngModel.splice(scope.ngModel.indexOf(layer), 1);
        };

        scope.dataModel = function () {
          return { id: null, from: null, to: null, rebate: null };
        };

        scope.addModel = function () {
          var model = scope.dataModel();
          model.id = angular.copy(scope.index);
          scope.ngModel.push(model)
          scope.index++;
          setDirty();
        }

        scope.hasNew = function () {
          var isNew, emptyLayer;
          isNew = false;
          if (!scope.ngModel) {
            scope.ngModel = [];
          }
          scope.ngModel.forEach(function (layer) {
            emptyLayer = true;
            //setDirty();
            Object.keys(scope.dataModel()).forEach(function (key) {
              if (key !== 'id' && layer[key] !== null) emptyLayer = false;
            });
            if (emptyLayer) isNew = true;
          });

          return isNew;
        }
        scope.sorterMethod = function (data) {
          if (data.from) {
            return Number(data.from);
          }
          return data.from;
        }
        scope.$watch('readMode', function (newValue, oldValue, scope) {
          if (newValue === true) {
            scope.ngModel = $filter('orderBy')(scope.ngModel, scope.sorterMethod);
          }
        });
        scope.$watch('ngModel', function (newValue, oldValue, scope) {
          if (scope.readMode === true && JSON.stringify(newValue) !== JSON.stringify(oldValue) ) {
            scope.ngModel = $filter('orderBy')(newValue, scope.sorterMethod);
          }
        });

        if (scope.ngModel.length <= 0) {
          scope.addModel();
        }
      }
    };
  };
  theDirective.$inject = ['$filter'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtMultiSelectForm';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtMultiSelectFrom.html',
        require: [],
        scope: {
            ngDisabled: '=',
            ngModel: '=',
            readMode: '=?',
            hideSelectAll:'=?',
            hasError: '=?',
            errorMessage: '@',
            errorPlacement: '@',
            placeholder: '@',
            label: '@?',
            name: '@',
            colClass: '=?',
            displayLocation: '@?',
            rowClass: '@?',
            ngModelPropertie:"@",
            ngModelBased:'@?',
            showSearchbar:'@?',
            displayProperty:'@?',
            isLoading:'=?',
            emptyText:'@?',
            source:'=',
            selectAllLabel:'@?',
            allSelected:'=?',
            selectAllMessage:'@?',
            isflat:'@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }
                if (!item) {
                  return null;
                }
                return item[scope.ngModelPropertie]
                }

                return "";
            }

            function specialFn(event,obj){
              var target = $(event.target);
              var el = $(element.find('cwt-multi-select'));
              if (el.has(target).length === 0 && $(el[0]).isolateScope()) {
                $(el[0]).isolateScope().ctrlMultiSelect.clickHandler(event);
              }
            }

            var form = $(element).closest('cwt-form-wrapper');

            if (form && form.length > 0 && angular.element(form).scope()){
              var ctrl = angular.element(form).scope().controller;
              if(ctrl && ctrl.addSpecialLiseners){
                ctrl.addSpecialLiseners(specialFn);
              }              
            }          
        }        
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtNumberFieldText';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        require:'ngModel',
        templateUrl: 'templates/cwtNumberInputfield.html',
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@?',
           name: '@',
           colClass: '=?',
           rowClass: '@?',
           ngModelPropertie:"@",
           ngModelBased:'@?',
           addon:'@?',
           focusFn: '&?',
           paddingLeft: '=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }

          if (!scope.addon) {
            scope.addon = '';
          }

          scope.showAddon = function(){
            if(scope.addon && scope.addon.length > 0){
              return true;
            }
            return false;
          }

          controller.$formatters.push(function(value) {
            var parsed = parseFloat(value);
            if(isNaN(parsed)){
              return undefined;
            }
            return parsed;
          });

          scope.ngChangefire = function(){
            if (attrs.ngChange) {
              var attrCopy = attrs.ngChange;
              $timeout(function(){
                scope.$parent.$eval(attrCopy);
              },0);
            }
          }

          scope.ngModelWithPropertie  = function(){
              if(scope.ngModel){
                var item = scope.ngModel;
                if(scope.ngModelBased) {
                    item = _.find(scope.data,function(o){
                    return o[scope.ngModelBased] === scope.ngModel;
                    });
                }              
                return item[scope.ngModelPropertie]
                }

                return "";
            }
          
        }        
    };
  };
  theDirective.$inject = ["$timeout"];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtRadioboxField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtRadioboxfield.html',
        require: [],
        scope: {
            ngDisabled:'=',
            ngModel:'=',
            readMode:'=?',
            errorMessage:'@',
            errorPlacement:'@',
            label:'@',
            ngClick:'&',
            isLoading:'=',
            data:'=',
            name:'@',
            colClass:'=?',
            rowClass:'=?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithPropertie  = function(){
            if(scope.ngModel){
              return scope.ngModel[scope.ngModelPropertie]
            }
            return "";
          }

          if(Array.isArray(scope.ngModel)){
            scope.ngModel = "";
          }
          scope.dataHelp = [];
          function resetData(){
            scope.dataHelp = [];
          }

          scope.$watch('ngModel',function(){
            resetData();
            if(scope.ngModel){
                var index = scope.data.indexOf(scope.ngModel);
                if(index > -1){
                  if(!scope.dataHelp[index]){
                    scope.dataHelp[index] = {};
                  }
                  scope.dataHelp[index].isChecked = true;
                }
            }
          });

          scope.selectionChanged = function($event,item){
           /* if(scope.ngModel === item){
              scope.ngModel = null;
            }  else {
              scope.ngModel = item;
            }*/
          }          
          
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'restrictInput';
    var theDirective = function () {
        return {
        restrict: 'A',
        priority:999,
        require: [],
        link: function (scope, element, attrs, controller) {
            var ele = $(element[0]);
            var regex = /(^[0-9]{1,14})(\.[0-9]{1,4})?$/i;

            String.prototype.insertAt = function(index, string) { 
                return this.substr(0, index) + string + this.substr(index);
            }

            function remove(str, startIndex, count) {
                return str.substr(0, startIndex) + str.substr(startIndex + count);
            }

            function getCaretPos(input) {

                var caret = {
                    start: -1,
                    stop: -1
                }

                // Internet Explorer Caret Position (TextArea)
                if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    var bookmark = range.getBookmark();
                    caret.start = bookmark.charCodeAt(2) - 2;
                    caret.stop = input.selectionEnd;
                } else {
                    // Firefox Caret Position (TextArea)
                    if (input.setSelectionRange)
                         caret.start = input.selectionStart;
                         caret.stop = input.selectionEnd;
                }

                return caret;
            }

            if (ele.type !== "number") {
                ele.bind('keypress',function(e){
                    var charCode = String.fromCharCode(e.keyCode || e.which);
                    var key = e.key || charCode;
                    if(!key) {
                        return;
                    }
                    var start = getCaretPos($(this)[0]).start;
                    var stop = getCaretPos($(this)[0]).stop;
                    var value = $(this).val();
                    if(start !== stop) {
                        value = remove(value, start, (stop - start));
                    }
                    value = value.insertAt(start,key);
                    if(key === '.'){
                        if ((value.match(new RegExp("[.]", "g")) || []).length > 1) {
                        e.preventDefault(); 
                        }
                    } else if (!RegExp(regex).test(value)){
                        e.preventDefault();
                    }
                });
            }

            scope.$on("$destroy", function(){
               ele.unbind('keypress');
            });   
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtSelectAsyncField';
    var theDirective = function ($timeout) {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtSelectAsyncField.html',
        require: [],
        scope: {
          ngDisabled:'=',
          ngModel:'=',
          readMode:'=?',
          hasError:'=?',
          errorMessage:'@',
          errorPlacement:'@',
          label:'@?',
          dataPromise:'=data',
          ngModelProperty:"@",
          options:'@',
          colClass: '=?',
          rowClass: '@?',
          name:'@',
          ngModelBased:'@?',
          defaultOption:'@?'
        },
        link: function (scope, element, attrs, controller) {

          scope.data = [];

          scope.isLoading = true;
          scope.$watch('dataPromise', function() {
            scope.isLoading = true;
            scope.dataPromise.then(function(options) {
              scope.data = options;
              scope.isLoading = false;
            });
          });


          //owner.name for owner in ngModel track by owner.id
          scope.ngModelWithProperty  = function(){
            if(scope.ngModel){
              var item = scope.ngModel;
              if(scope.ngModelBased) {
                item = _.find(scope.data,function(o){
                  return o[scope.ngModelBased] === scope.ngModel;
                });
              }
              return item ? item[scope.ngModelProperty] : '-'
            }
            return "-";
          }

          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
        }
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function (module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
  var directiveName = 'cwtSelectField';
  var theDirective = function ($timeout) {
    return {
      restrict: 'E',
      templateUrl: 'templates/cwtSelectfield.html',
      require: [],
      scope: {
        ngDisabled: '=',
        ngModel: '=',
        readMode: '=?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        label: '@?',
        isLoading: '=',
        data: '=',
        ngModelPropertie: "@",
        options: '@',
        optionsFilter: '<?',
        colClass: '=?',
        rowClass: '@?',
        name: '@',
        notActiveInForm: '@?',
        ngModelBased: '@?',
        defaultOption: '@?',
        ngChange: '&?',
        changeFn: '=?'
      },
      link: function (scope, element, attrs, controller) {
        //owner.name for owner in ngModel track by owner.id
        scope.ngModelWithPropertie = function () {
          if (scope.ngModel) {
            var item = scope.ngModel;
            if (scope.ngModelBased) {
              item = _.find(scope.data, function (o) {
                return o[scope.ngModelBased] === scope.ngModel;
              });
            }
            if (!item) {
              return null;
            }
            return item[scope.ngModelPropertie]
          }

          return "";
        }

        scope.change = function (obj) {
          if (scope.ngChange) {
            scope.ngChange({ data: obj });
          }
          if (scope.changeFn) {
            scope.changeFn(obj);
          }
        }

        if (!scope.colClass) {
          scope.colClass = {
            field: 'col-md-8',
            label: 'col-md-4'
          }
        }
        if (!scope.rowClass) {
          scope.rowClass = 'form-group row';
        }
      }
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtTextAreaField';
    var theDirective = function () {
        return {
        restrict: 'E',
        templateUrl: 'templates/cwtTextAreaField.html',
        require: [],
        scope: {
           ngDisabled: '=',
           ngModel: '=',
           readMode: '=?',
           hasError: '=?',
           errorMessage: '@',
           errorPlacement: '@',
           placeholder: '@',
           label: '@',
           colClass: '=?',
           rowClass: '@?'
        },
        link: function (scope, element, attrs, controller) {
          if(!scope.colClass){
            scope.colClass = {
              field: 'col-md-8',
              label: 'col-md-4'
            }
          }
          if(!scope.rowClass){
            scope.rowClass = 'form-group row';
          }
        }
    };
  };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function(module) {
  try {
    module = angular.module('cwt.form-components');
  } catch (e) {
    module = angular.module('cwt.form-components', []);
  }
    var directiveName = 'cwtTypeahead';
    var theDirective = function () {
        return {
          restrict: 'E',
          templateUrl: 'templates/cwtTypeahead.html',
          require: [],
          scope: {
             ngDisabled: '=',
             ngModel: '=',
             readMode: '=?',
             hasError: '=?',
             errorMessage: '@',
             errorPlacement: '@',
             placeholder: '@',
             label: '@?',
             name: '@',
             colClass: '=?',
             rowClass: '@?',
             typeaheadWaitMs:"@",
             typeaheadMinLength:"@",
             uibTypeahead:"@",
             typeaheadPromise:"&",
             data:"=",
             ngModelPropertie:"@",
             ngModelBased:'@?'
          },
          compile: function(tElem,tAttr){
              $(tElem).find('input').attr('uib-typeahead',tAttr.uibTypeahead);
              $(tElem).find('input').attr('typeahead-min-length',tAttr.typeaheadMinLength);
              $(tElem).find('input').attr('typeahead-wait-ms',tAttr.typeaheadWaitMs);
              return {
                  post:function (scope, element, attrs, controller) {
                      element.find('input').attr('uib-typeahead',scope.uibTypeahead);
                      if(!scope.colClass){
                          scope.colClass = {
                          field: 'col-md-8',
                          label: 'col-md-4'
                          }
                      }
                      if(!scope.rowClass){
                          scope.rowClass = 'form-group row';
                      }

                      scope.uibTypeaheadPromise = function(val){
                        return scope.typeaheadPromise({$viewValue:val});
                      }

                      scope.ngModelWithPropertie  = function(){
                          if(scope.ngModel){
                          var item = scope.ngModel;
                          if(scope.ngModelBased) {
                              item = _.find(scope.data,function(o){
                              return o[scope.ngModelBased] === scope.ngModel;
                              });
                          }              
                          return item[scope.ngModelPropertie]
                          }

                          return "";
                      }
                  }
              }
          }
        }
    };
  theDirective.$inject = [];
  module.directive(directiveName, theDirective);
  module.filter('sortTypeAhead', function () {
    return function (data, input, sortFirst, sortSecond) {
      var first = [];
      var others = [];
      if (sortFirst && data && data[0] && data[0][sortFirst]) {
        for (var i = 0; i < data.length; i++) {
          if (data[i][sortFirst].toLowerCase().indexOf(input.toLowerCase()) == 0) {
              first.push(data[i]);
          } else {
              others.push(data[i]);
          }
        }
        first = _.sortBy(first, function(o){return o[sortFirst]});
        others = sortSecond ? _.sortBy(others, function(o){return o[sortSecond]}) : others;
        return (first.concat(others));
      } else {
        return data;
      }
      
    }
  });
})(null);
;angular.module('cwt.form-components').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/cwtCheckboxesfield.html',
    "<div ng-show=!readMode class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<span ng-show=isLoading><i class=\"fa fa-spinner fa-pulse fa-fw fa-span\"></i></span>\n" +
    "<div class=checkbox ng-show=!isLoading>\n" +
    "<label unselectable=on class=unselectable for={{type.name}}-{{$id}} data-ng-repeat=\"type in data\">\n" +
    "<input type=checkbox checked name={{type.name}}-{{$id}} id={{type.name}}-{{$id}} value={{type.id}} ng-model=dataHelp[$index].isChecked ng-click=\"selectionChanged($event, type)\">\n" +
    "<span class=checkboxItem>{{type.name}}</span>\n" +
    "</label>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div class={{colClass.field}}>\n" +
    "<span class=\"form-label-text checkboxItem\" ng-class=\"{unselected: !dataHelp[$index].isChecked}\" data-ng-repeat=\"type in data\">\n" +
    "{{type.name}}\n" +
    "</span>\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtCheckboxfield.html',
    "<div ng-show=!readMode class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<span ng-show=isLoading><i class=\"fa fa-spinner fa-pulse fa-fw fa-span\"></i></span>\n" +
    "<div class=checkbox ng-show=!isLoading>\n" +
    "<label unselectable=on class=unselectable style=\"line-height: 40px\" for={{id}}-{{$id}}>\n" +
    "<input type=checkbox name={{id}}-{{$id}} id={{id}}-{{$id}} ng-model=ngModel>\n" +
    "<span class=checkboxItem></span>\n" +
    "</label>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div class={{colClass.field}}>\n" +
    "<span class=form-label-text ng-if=ngModel>Yes</span>\n" +
    "<span class=form-label-text ng-if=!ngModel>No</span>\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtDatepicker.html',
    "<div class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div ng-show=!readMode class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter outsideClick'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<cwt-date-picker data-placeholder=mm/dd/yyyy data-has-error=hasError ngdisabled=ngDisabled data-date-picker-model=ngModel data-date-picker-output-model=outputModel></cwt-date-picker>\n" +
    "</div>\n" +
    "<span class=form-label-text ng-show=readMode>{{(outputModel | date : 'dd MMM yyyy' : 'UTC') || \"-\" }}</span>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtForm.html',
    "<form ng-disabled=true novalidate autocomplete=off ng-class=\"{'not-active':notActive}\">\n" +
    "<div class=clearfix>\n" +
    "<div ng-show=\"saving || isLoading\" class=saving-overlay><i class=\"fa fa-spinner fa-spin fa-2x saving-spinner\" aria-hidden=true></i></div>\n" +
    "<ng-transclude></ng-transclude>\n" +
    "</div>\n" +
    "<div ng-show=!readMode class=form-footer>\n" +
    "<button class=\"btn btn-primary form-footer-button-save\" ng-click=saveChanges() ng-disabled=\"saving || !this[name].$dirty\">Save</button>\n" +
    "<button class=\"btn btn-primary form-footer-button-reset\" ng-click=resetForm() ng-disabled=\"saving || !this[name].$dirty\">Reset</button>\n" +
    "<button class=\"btn btn-primary form-footer-button-cancel\" ng-disabled=\"saving || required\" ng-click=cancelForm()>Cancel</button>\n" +
    "<button class=\"btn btn-primary\" ng-show=false ng-click=undoChanges() ng-disabled=\"index < 0\">Undo</button>\n" +
    "<button class=\"btn btn-primary\" ng-show=false ng-click=redoChanges() ng-disabled=\"index != originalObject.objecList.length - 1\">Redo</button>\n" +
    "</div>\n" +
    "</form>"
  );


  $templateCache.put('templates/cwtFormElementWrapper.html',
    "<span>\n" +
    "<ng-transclude></ng-transclude>\n" +
    "</span>"
  );


  $templateCache.put('templates/cwtInflectionField.html',
    "<div class=layer-field style=\"border: 0px; padding: 0px\" ng-class=\"{'layer-error': hasError,'has-feedback': hasError}\" uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<table class=\"table table-striped\" style=\"background: #fff\">\n" +
    "<tbody>\n" +
    "<tr ng-repeat=\"inflectionPoint in ngModel track by inflectionPoint.id\" class=row>\n" +
    "<td class=col-md-6>\n" +
    "<cwt-number-field-text ng-hide=readMode row-class=\"form-group row\" col-class=\"{label:'col-md-4', field:'col-md-11'}\" data-ng-model=inflectionPoint.target placeholder=\"\" addon={{targetAddon}} focus-fn=selectRow(inflectionPoint.id) keep-focus row-id={{inflectionPoint.id}} selected-row-id={{selectedRowId}} index={{$index}}>\n" +
    "</cwt-number-field-text>\n" +
    "<span ng-show=readMode ng-bind=inflectionPoint.target></span><span ng-show=readMode>&nbsp;</span><span ng-show=readMode ng-bind=targetAddon></span>\n" +
    "</td>\n" +
    "<td class=col-md-5>\n" +
    "<cwt-number-field-text ng-hide=readMode row-class=\"form-group row\" col-class=\"{label:'col-md-4', field:'col-md-12'}\" data-ng-model=inflectionPoint.rebate placeholder=\"\" addon={{rebateAddon}}>\n" +
    "</cwt-number-field-text>\n" +
    "<span ng-show=readMode ng-bind=inflectionPoint.rebate></span><span ng-show=readMode>&nbsp;</span><span ng-show=readMode ng-bind=rebateAddon></span>\n" +
    "</td>\n" +
    "<td class=col-md-1>\n" +
    "<button type=button class=\"btn btn-danger\" name=button ng-hide=readMode ng-click=remove(inflectionPoint) style=width:10px>\n" +
    "<i style=\"color:white;margin-left: -7px\" class=\"fa fa-trash-o remove-trash\"></i>\n" +
    "</button>\n" +
    "</td>\n" +
    "</tr>\n" +
    "</tbody>\n" +
    "</table>\n" +
    "<div class=text-center>\n" +
    "<button ng-hide=readMode type=button class=\"btn btn-success\" ng-click=addModel() ng-disabled=hasNew()>\n" +
    "<i class=\"fa fa-plus\"></i>\n" +
    "Add Inflection Point\n" +
    "</button>\n" +
    "</div>\n" +
    "</div>\n"
  );


  $templateCache.put('templates/cwtInputfield.html',
    "<div ng-show=!readMode ng-class=\"{'has-error': hasError,'has-feedback': hasError}\" class={{rowClass}}>\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<input maxlength=255 id={{name}} name={{name}} class=form-control placeholder={{placeholder}} ng-disabled=ngDisabled ng-model=ngModel>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=\"ngModel && !ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModel}}\n" +
    "</div>\n" +
    "<div ng-show=\"ngModel && ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModelWithPropertie()}}\n" +
    "</div>\n" +
    "<div ng-show=!ngModel class=\"form-label-text {{colClass.field}}\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>\n"
  );


  $templateCache.put('templates/cwtLayerField.html',
    "<div class=layer-field ng-class=\"{'layer-error': hasError,'has-feedback': hasError}\" uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<table class=\"table table-striped\" style=\"background: #fff\">\n" +
    "<thead>\n" +
    "<tr class=row>\n" +
    "<th class=col-md-5>{{targetLabel}} (from value)</th>\n" +
    "<th class=col-md-5>{{rebateLabel}}</th>\n" +
    "<th class=col-md-2>&nbsp</th>\n" +
    "</tr>\n" +
    "</thead>\n" +
    "<tbody>\n" +
    "<tr ng-repeat=\"layer in ngModel track by layer.id\" class=row>\n" +
    "<td class=col-md-5>\n" +
    "<cwt-number-field-text ng-hide=readMode row-class=\"form-group row\" col-class=\"{label:'col-md-4', field:'col-md-10'}\" data-ng-model=layer.from placeholder=\"\" addon={{targetAddon}} focus-fn=selectRow(layer.id) keep-focus row-id={{layer.id}} selected-row-id={{selectedRowId}} index={{$index}}>\n" +
    "</cwt-number-field-text>\n" +
    "<span ng-show=readMode ng-bind=layer.from></span><span ng-show=readMode>&nbsp;</span><span ng-show=readMode ng-bind=targetAddon></span>\n" +
    "</td>\n" +
    "<td class=col-md-5>\n" +
    "<cwt-number-field-text ng-hide=readMode row-class=\"form-group row\" col-class=\"{label:'col-md-4', field:'col-md-10'}\" data-ng-model=layer.rebate placeholder=\"\" addon={{rebateAddon}}>\n" +
    "</cwt-number-field-text>\n" +
    "<span ng-show=readMode ng-bind=layer.rebate></span><span ng-show=readMode>&nbsp;</span><span ng-show=readMode ng-bind=rebateAddon></span>\n" +
    "</td>\n" +
    "<td class=col-md-2>\n" +
    "<button type=button class=\"btn btn-danger\" name=button ng-hide=readMode ng-click=remove(layer)>\n" +
    "<i style=color:white class=\"fa fa-trash-o remove-trash\"></i>\n" +
    "</button>\n" +
    "</td>\n" +
    "</tr>\n" +
    "</tbody>\n" +
    "</table>\n" +
    "<div class=text-center>\n" +
    "<button ng-hide=readMode type=button class=\"btn btn-success\" ng-click=addModel() ng-disabled=hasNew()>\n" +
    "<i class=\"fa fa-plus\"></i>\n" +
    "Add Layer\n" +
    "</button>\n" +
    "</div>\n" +
    "</div>\n"
  );


  $templateCache.put('templates/cwtMultiSelectFrom.html',
    "<div ng-show=!readMode ng-class=\"{'has-error': hasError,'has-feedback': hasError}\" class=\"{{scope.rowClass}} form-group\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<cwt-multi-select name={{name}} id={{id}} source=source ng-model=ngModel empty-text={{emptyText}} readonly-mode=readMode col-class=colClass hide-select-all=hideSelectAll display-location={{displayLocation}} loading-data=isLoading display-property={{displayProperty}} show-searchbar=showSearchbar select-all-label={{selectAllLabel}} all-selected=allSelected select-all-message={{selectAllMessage}} isflat={{isflat}}>\n" +
    "</cwt-multi-select>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=ngModel ng-show=!ngModelPropertie class=\"{{colClass.field}} form-label-text\">\n" +
    "<span ng-if=allSelected>{{selectAllMessage}}</span>\n" +
    "<cwt-multi-select ng-show=!allSelected source=source ng-model=ngModel readonly-mode=readMode col-class=colClass hide-select-all=hideSelectAll empty-text={{emptyText}} loading-data=isLoading name={{name}} id={{id}} display-property={{displayProperty}} show-searchbar=showSearchbar select-all-label={{selectAllLabel}} all-selected=allSelected select-all-message={{selectAllMessage}} isflat={{isflat}}>\n" +
    "</cwt-multi-select>\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtNumberInputfield.html',
    "<div style=margin:0px ng-show=!readMode ng-class=\"{'has-error': hasError,'has-feedback': hasError}\" class={{rowClass}}>\n" +
    "<div class=\"input-group cwt-number-input-field\" ng-class=colClass.field uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<input ng-change=ngChangefire() ng-class=\"{'no-add-on': !(addon && addon.length > 0)}\" restrict-input=/^[0-9]{1,14}(.[0-9]{1,4})?$/ id={{name}} name={{name}} class=form-control placeholder={{placeholder}} ng-disabled=ngDisabled ng-model=ngModel ng-focus=focusFn()>\n" +
    "<span ng-show=showAddon() class=input-group-addon>{{addon}}</span>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=\"(ngModel || ngModel === 0) && !ngModelPropertie\" class=\"{{paddingLeft ? '' : colClass.field}} form-label-text\">\n" +
    "{{ngModel}} {{addon}}\n" +
    "</div>\n" +
    "<div ng-show=\"(ngModel || ngModel === 0) && ngModelPropertie\" class=\"{{paddingLeft ? '' : colClass.field}} form-label-text\">\n" +
    "{{ngModelWithPropertie()}} {{addon}}\n" +
    "</div>\n" +
    "<div ng-show=\"!(ngModel || ngModel === 0) \" class=\"form-label-text {{paddingLeft ? '' : colClass.field}}\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtRadioboxField.html',
    "<div class=form-group class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div ng-show=!readMode class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<span ng-show=isLoading><i class=\"fa fa-spinner fa-pulse fa-fw fa-span\"></i></span>\n" +
    "<div class=checkbox ng-show=!isLoading>\n" +
    "<label unselectable=on class=unselectable style=\"padding-left: 0px\" for={{type.name}}-{{$id}} data-ng-repeat=\"type in data\">\n" +
    "<input type=radio name={{name}} id={{type.name}}-{{$id}} ng-model=$parent.ngModel ng-value=type ng-click=\"selectionChanged($event, type)\">\n" +
    "<span class=checkboxItem>\n" +
    "{{type.name}}\n" +
    "</span>\n" +
    "</label>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=ngModel class=\"{{colClass.field}} form-label-text\">\n" +
    "<span ng-show=readMode class=\"form-label-text checkboxItem\" ng-class=\"{unselected: !dataHelp[$index].isChecked}\" data-ng-repeat=\"type in data\">\n" +
    "{{type.name}}\n" +
    "</span>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtSelectAsyncField.html',
    "<div ng-show=!readMode class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<div ng-show=isLoading><span class=\"form-control dropdown-loader\"><i class=\"fa fa-spinner fa-pulse fa-fw\"></i></span></div>\n" +
    "<div ng-show=!isLoading>\n" +
    "<select id={{name}} name={{name}} class=form-control ng-model=ngModel ng-disabled=ngDisabled ng-options={{options}}>\n" +
    "<option ng-if=\"defaultOption !== undefined\" hidden value=\"\">{{defaultOption}}</option>\n" +
    "</select>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=\"ngModel && !ngModelProperty\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModel}}\n" +
    "</div>\n" +
    "<div ng-show=\"ngModel && ngModelProperty\" class=\"{{colClass.field}} form-label-text\">\n" +
    "<span ng-show=isLoading><i class=\"fa fa-spinner fa-pulse fa-fw\"></i></span>\n" +
    "<span ng-if=!isLoading>{{ngModelWithProperty()}}</span>\n" +
    "</div>\n" +
    "<div ng-show=!ngModel class=\"{{colClass.field}} form-label-text\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>\n"
  );


  $templateCache.put('templates/cwtSelectfield.html',
    "<div ng-show=!readMode class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<div ng-show=isLoading><span class=\"form-control dropdown-loader\"><i class=\"fa fa-spinner fa-pulse fa-fw\"></i></span></div>\n" +
    "<div ng-show=!isLoading>\n" +
    "<select id={{name}} name={{name}} class=form-control ng-model=ngModel ng-disabled=ngDisabled ng-options={{options}} ng-change=change(ngModel)>\n" +
    "<option ng-if=\"defaultOption !== undefined\" hidden value=\"\">{{defaultOption}}</option>\n" +
    "</select>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=\"ngModel && !ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModel}}\n" +
    "</div>\n" +
    "<div ng-show=\"ngModel && ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ ngModelWithPropertie() || '-' }}\n" +
    "</div>\n" +
    "<div ng-show=!ngModel class=\"{{colClass.field}} form-label-text\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtTextAreaField.html',
    "<div ng-show=!readMode class={{rowClass}} ng-class=\"{'has-error': hasError,'has-feedback': hasError}\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<textarea class=form-control placeholder={{placeholder}} ng-disabled=ngDisabled ng-model=ngModel>\n" +
    "        \n" +
    "    </textarea>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=ngModel class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModel}}\n" +
    "</div>\n" +
    "<div ng-show=!ngModel class=\"{{colClass.field}} form-label-text\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/cwtTypeahead.html',
    "<div ng-show=!readMode ng-class=\"{'has-error': hasError,'has-feedback': hasError}\" class=\"{{scope.rowClass}} form-group\">\n" +
    "<div class={{colClass.field}} uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<input id={{name}} name={{name}} class=form-control placeholder={{placeholder}} ng-disabled=ngDisabled ng-model=ngModel typeahead-wait-ms=typeaheadWaitMs typeahead-min-length=typeaheadMinLength typeahead-loading=typeaheadWaitPromise>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div ng-show=readMode class={{rowClass}}>\n" +
    "<div ng-show=\"ngModel && !ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModel}}\n" +
    "</div>\n" +
    "<div ng-show=\"ngModel && ngModelPropertie\" class=\"{{colClass.field}} form-label-text\">\n" +
    "{{ngModelWithPropertie()}}\n" +
    "</div>\n" +
    "<div ng-show=!ngModel class=\"form-label-text {{colClass.field}}\">\n" +
    "-\n" +
    "</div>\n" +
    "</div>"
  );

}]);
